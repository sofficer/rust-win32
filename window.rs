use io::ReaderUtil;
use libc::{ DWORD, c_int, c_uint, c_long, LPCWSTR, HANDLE, LPVOID, BOOL };
use os::win32::as_utf16_p;
use core::either::Either;

#[abi = "stdcall"]
extern mod kernel32 {
	fn GetLastError() -> DWORD;
}

struct POINT {
	mut x: c_long,
	mut y: c_long
}

struct MSG {
	mut hWnd: HANDLE,
	mut message: c_uint,
	mut wParam: c_uint,
	mut lParam: c_uint,
	mut time: DWORD,
	mut point: POINT
}

type LPMSG = *MSG;


#[abi = "stdcall"]
extern mod user32 {
	fn CreateWindowExW(
		extendedStyle: DWORD,
		className: LPCWSTR, 
		windowName: LPCWSTR, 
		style: DWORD,
		x: c_int,
		y: c_int,
		width: c_int,
		height: c_int,
		parent: HANDLE,
		menu: HANDLE,
		instance: HANDLE,
		lParam: LPVOID
	) -> HANDLE;
	
	fn ShowWindow(hWnd: HANDLE, cmdShow: c_int) -> BOOL;
	
	fn GetMessageW(message: LPMSG, hWnd: HANDLE, MsgFilterMin: c_uint, MsgFilterMax: c_uint) -> BOOL;
	
	fn TranslateMessage(message: LPMSG) -> BOOL;
	fn DispatchMessageW(message: LPMSG) -> BOOL;
}

fn CreateWindow(classname: &str, windowname: &str) -> Either<HANDLE, DWORD> {
	let parent: HANDLE = ptr::mut_null();
	let menu: HANDLE = ptr::mut_null();
	let instance: HANDLE = ptr::mut_null();
	let lParam: LPVOID = ptr::mut_null();
	
	let handle = 
		do as_utf16_p(classname) |class_name| {
			do as_utf16_p(windowname) |window_name| {
				user32::CreateWindowExW(
					256,
					class_name, 
					window_name, 
					0x00C00000 | 0x00080000 | 0x00040000 | 0x00020000 | 0x00010000, 
					10, 
					10, 
					400, 
					400, 
					parent, 
					menu, 
					instance, 
					lParam
				)
			}
		};
	if handle == ptr::null() {
		let error_code = kernel32::GetLastError();
		return either::Right(error_code);
	} else {
		return either::Left(handle);
	}
}

fn WindowSuccess(handle: HANDLE) -> () {
	io::println(fmt!("Got handle: %?", handle));
	user32::ShowWindow(handle, 5);
	
	let msg = MSG {
		mut hWnd: ptr::mut_null(),
		mut message: 0,
		mut wParam: 0,
		mut lParam: 0,
		mut time: 0,
		mut point: POINT { mut x: 0, mut y: 0 }
	};
	loop {
		let result = user32::GetMessageW(ptr::addr_of(&msg), handle, 0, 0);
		match result {
			-1 => {
				let err_code = kernel32::GetLastError();
				io::println(fmt!("Didn't get message: %?", err_code));
				break;
			},
			0 => {
				io::println("Got WM_QUIT message");
				break;
			},
			_ => {
				user32::TranslateMessage(ptr::addr_of(&msg));
				io::println("Got some other message");
				io::println(fmt!("%?", msg.message));
				user32::DispatchMessageW(ptr::addr_of(&msg));
			}
		}
	}
}

fn main() {
	match CreateWindow(&"Button", &"Hello World") {
		either::Left(handle) => WindowSuccess(handle),
		either::Right(err_code) => io::println(fmt!("Oops error: %?", err_code))
	}
}